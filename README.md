# How to run
```
cd docker
docker-compose up -d --build
```
If you wish, you may adjust ports or other things in `docker-compose.yaml` and `docker/.env`.

You may then run (inside "php" container):
```
composer install
bin/console lexik:jwt:generate-keypair
```

Now adjust Symfony `.env` file.

# Migrations
```
bin/console doctrine:m:m
```

# Authorization

See `http-tests/01-register-user` and `http-tests/02-obtaining-access-token`

Write down created user id and use it in other tests to play with API.

# Functions

* rest api for registering users and adding/editing their profiles
* generating csv with user profiles via command (see `bin/console list`)

# Nice to add

* EventListener to parse exceptions to json format (like in FOSRestBundle)
* Functional tests
* Custom exceptions not just RuntimeException
* Separate more logic in controllers and command
