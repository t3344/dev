<?php

declare(strict_types=1);

namespace App\Controller\ApiV1;

use App\Entity\User;
use App\Service\Instantiator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/users')]
class UserController extends AbstractController
{
    #[Route(name: 'register_user', methods: ['POST'])]
    public function register(
        Request $request,
        EntityManagerInterface $manager,
        UserPasswordHasherInterface $passwordHasher,
        Instantiator $instantiator
    ): JsonResponse {
        /** @var User $user */
        $user = $instantiator->deserialize($request->getContent(), User::class);

        $existingUser = $manager->getRepository(User::class)
            ->findOneBy(['username' => $user->getUsername()])
        ;
        if ($existingUser) {
            throw new \RuntimeException('User already exists.');
        }

        $password = $passwordHasher->hashPassword($user, $user->getPassword());
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        return $this->json(
            ['user_id' => $user->getId()],
            Response::HTTP_CREATED
        );
    }
}
