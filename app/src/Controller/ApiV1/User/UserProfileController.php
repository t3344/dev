<?php

declare(strict_types=1);

namespace App\Controller\ApiV1\User;

use App\Entity\User;
use App\Entity\UserProfile;
use App\Service\Instantiator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/users/{id}/profile', name: 'user_profile_')]
class UserProfileController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Instantiator $instantiator
    ) {}

    #[Route(name: 'get', methods: ['GET'])]
    public function get(User $user): JsonResponse
    {
        if ($this->getUser()->getUserIdentifier() !== $user->getUserIdentifier()) {
            throw new \RuntimeException('No access.');
        }

        if (!$user->getUserProfile()) {
            throw new \RuntimeException('Profile does not exist. Please use POST method.');
        }

        return $this->json($user->getUserProfile());
    }

    #[Route(name: 'create', methods: ['POST'])]
    public function create(User $user, Request $request): JsonResponse
    {
        if ($this->getUser()->getUserIdentifier() !== $user->getUserIdentifier()) {
            throw new \RuntimeException('No access.');
        }

        if ($user->getUserProfile()) {
            throw new \RuntimeException('Profile already exists.');
        }

        /** @var UserProfile $profile */
        $profile = $this->instantiator->deserialize(
            $request->getContent(),
            UserProfile::class
        );
        $profile->setBoundUser($user);

        $this->entityManager->persist($profile);
        $this->entityManager->flush();

        return $this->json(
            ['profile_id' => $profile->getId()],
            Response::HTTP_CREATED
        );
    }

    #[Route(name: 'edit', methods: ['PUT'])]
    public function edit(User $user, Request $request)
    {
        if ($this->getUser()->getUserIdentifier() !== $user->getUserIdentifier()) {
            throw new \RuntimeException('No access.');
        }

        if (!$user->getUserProfile()) {
            throw new \RuntimeException('Profile does not exist. Please use POST method.');
        }

        $profile = $this->instantiator->deserialize(
            $request->getContent(),
            UserProfile::class,
            null,
            $user->getUserProfile()
        );

        $this->entityManager->persist($profile);
        $this->entityManager->flush();

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
