<?php

namespace App\Controller\ApiV1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/some-data')]
class SomeDataController extends AbstractController
{
    #[Route(name: 'some_data_index')]
    public function index(): JsonResponse
    {
        $user = $this->getUser();

        return $this->json([
            'zz' => $user->getUsername(),
        ]);
    }
}
