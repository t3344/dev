<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Entity\UserProfile;
use App\Entity\UserProfilesReport;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\Exporter\Handler;
use Sonata\Exporter\Source\DoctrineORMQuerySourceIterator;
use Sonata\Exporter\Writer\CsvWriter;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Uid\Uuid;

#[AsCommand(
    name: 'app:generate-users-csv',
    description: 'Generates csv file with user profiles.',
)]
class GenerateUsersCsvCommand extends Command
{
    private readonly EntityManagerInterface $entityManager;

    private readonly string $dirPath;

    public function __construct(EntityManagerInterface $manager, string $dirPath, string $name = null)
    {
        parent::__construct($name);

        $this->entityManager = $manager;
        $this->dirPath = $dirPath;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $query = $this->entityManager->getRepository(User::class)
            ->createQueryBuilder('u')
            ->getQuery()
        ;
        $source = new DoctrineORMQuerySourceIterator($query, [
            'username',
            'userProfile.firstname',
            'userProfile.lastname',
            'userProfile.email',
        ]);

        $dateInPath = sprintf('%d/%d', date('Y'), date('m'));
        $fileName = sprintf('%s.csv', Uuid::v4());
        $absolutePathWithDate = sprintf('%s/%s', $this->dirPath, $dateInPath);
        $filePath = sprintf('%s/%s', $absolutePathWithDate, $fileName);

        if (!is_dir($absolutePathWithDate)) {
            mkdir($absolutePathWithDate, 0775, true);
        }

        $writer = new CsvWriter($filePath);
        $handler = Handler::create($source, $writer);
        $handler->export();

        $report = new UserProfilesReport();
        $report->setRelativePath(sprintf('%s/%s', $dateInPath, $fileName));
        $this->entityManager->persist($report);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
