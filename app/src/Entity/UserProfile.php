<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserProfileRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints;

#[ORM\Entity(repositoryClass: UserProfileRepository::class)]
class UserProfile
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(length: 180, nullable: true)]
    #[Constraints\Type(type: Types::STRING)]
    #[Constraints\Length(min: 1, max: 180)]
    private ?string $firstname = null;

    #[ORM\Column(length: 180, nullable: true)]
    #[Constraints\Type(type: Types::STRING)]
    #[Constraints\Length(min: 1, max: 180)]
    private ?string $lastname = null;

    #[ORM\Column(length: 180, nullable: true)]
    #[Constraints\Email]
    private ?string $email = null;

    #[ORM\OneToOne(inversedBy: 'userProfile', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Ignore]
    private User $boundUser;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBoundUser(): User
    {
        return $this->boundUser;
    }

    public function setBoundUser(User $boundUser): self
    {
        $this->boundUser = $boundUser;

        return $this;
    }
}
