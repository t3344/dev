<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;

class Instantiator
{
    public function __construct(
        protected SerializerInterface $serializer,
        protected ValidatorInterface $validator
    ) {}

    public function deserialize(
        string $data,
        string $class,
        ?string $group = null,
        ?object $existingObject = null
    ) {
        $options = [];

        if ($group) {
            $options[AbstractNormalizer::GROUPS] = [$group];
        }

        if ($existingObject) {
            $options[AbstractNormalizer::OBJECT_TO_POPULATE] = $existingObject;
        }

        try {
            $instance = $this->serializer->deserialize(
                $data,
                $class,
                'json',
                $options
            );
        } catch (NotNormalizableValueException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        $this->validate($instance, $group);

        return $instance;
    }

    private function validate($instance, ?string $group = null): void
    {
        $groups = [];

        if ($group) {
            $groups[] = $group;
        }

        $violations = $this->validator->validate($instance, null, $groups);
        if ($violations->count() > 0) {
            throw new \RuntimeException();
        }
    }
}
