<?php

declare(strict_types=1);

namespace App\EventListener\LexikJWTAuthentication;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

final class AuthenticationSuccessEventListener
{
    public function __invoke(AuthenticationSuccessEvent $event): void
    {
        $data = $event->getData();

        /** @var User $user */
        $user = $event->getUser();

        $data['user_id'] = $user->getId();

        $event->setData($data);
    }
}
